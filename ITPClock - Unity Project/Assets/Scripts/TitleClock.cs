﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TitleClock : MonoBehaviour
{

    public Transform secHand, minHand, hourHand;
    public int secSpeed, minSpeed, hourSpeed;
    // Start is called before the first frame update
    void Start()
    {
        hourHand.rotation =  Quaternion.Euler(0, 0, 0);
        minHand.rotation = Quaternion.Euler(0, 0, 0);
        secHand.rotation = Quaternion.Euler(0, 0, 0);

        
    }

    // Update is called once per frame
    void Update()
    {
        SpinHands();
    }

    public void SpinHands()
    {
        hourHand.Rotate(0, 0, hourSpeed);
        minHand.Rotate(0, 0, minSpeed);
        secHand.Rotate(0, 0, secSpeed);
    }
}
