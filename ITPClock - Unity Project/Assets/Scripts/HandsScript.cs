using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandsScript : MonoBehaviour
{
    public GameObject hourHand, minHand, secHand, secButInc, secButDec, secText, secColon, multiButton;
    private int buttonPressed, indicator, sHandMove, incValue;
    private BulkIncreaseScript bulkScript;
    public bool secToggle;
    private float minRotation, hourRotation, hourRelativeRotation, mHandMove, hHandFloat, mHandFloat, hHandMove;    
    public RectTransform hourHandRect, minHandRect, secHandRect;
    void Start()
    {
        // hourHandRect = hourHand.GetComponent<RectTransform>();
        // minHandRect = minHand.GetComponent<RectTransform>();
        // secHandRect = secHand.GetComponent<RectTransform>();
        ShowSeconds(true);
        bulkScript = multiButton.GetComponent<BulkIncreaseScript>();
    }
    public void ResetHands()
    {
       hourHandRect.transform.rotation =  Quaternion.Euler(0, 0, 0);
       minHandRect.transform.rotation = Quaternion.Euler(0, 0, 0);
       secHandRect.transform.rotation = Quaternion.Euler(0, 0, 0); 
    }

    public void MoveHands(int buttonPressed)
    {   
        minRotation = 6;
        hourRotation = 30;
        hourRelativeRotation = 0.5f;
        
        indicator = buttonPressed;
        switch(indicator)
        {
            case 1:
                hourRotation *= -1;
                hourHandRect.Rotate(0,0,hourRotation);
            break;

            case 2:
                hourHandRect.Rotate(0, 0, hourRotation);
            break;
            
            case 3:
                hourRelativeRotation *= -1;
                minRotation *= -1;
                hourHandRect.Rotate(0, 0, hourRelativeRotation);
                minHandRect.Rotate(0, 0, minRotation);
            break;

            case 4:
                hourHandRect.Rotate(0, 0, hourRelativeRotation);
                minHandRect.Rotate(0, 0, minRotation);
            break;
            
            case 5:
                minRotation *= -1;
                secHandRect.Rotate(0, 0, minRotation);
            break;

            case 6:
                secHandRect.Rotate(0, 0, minRotation);
            break;

            case 7:
                incValue = int.Parse(bulkScript.timeArray[bulkScript.currIncrament]);
                
                minRotation = (minRotation * incValue * -1);
                minHandRect.Rotate(0, 0, minRotation);

                hourRelativeRotation = (hourRelativeRotation * incValue * -1);
                hourHandRect.Rotate(0, 0, hourRelativeRotation);
            break;

            case 8:
                incValue = int.Parse(bulkScript.timeArray[bulkScript.currIncrament]);
                
                minRotation = (minRotation * incValue);
                minHandRect.Rotate(0, 0, minRotation);

                hourRelativeRotation = (hourRelativeRotation * incValue);
                hourHandRect.Rotate(0, 0, hourRelativeRotation);
            break;

            case 9:
                hourRotation *= -1;
                hourHandRect.Rotate(0,0,hourRotation);
            break;

            case 10:
                hourHandRect.Rotate(0,0,hourRotation);
            break;

            case 69:
                minRotation *= -1;
                secHandRect.Rotate(0, 0, minRotation);
            break;
        }
    }

    public void MatchHands(int hHand, int mHand, int sHand)
    {
        ResetHands();
        float hHandFloat = hHand;
        float mHandFloat = mHand;

        hHandMove = ((hHandFloat * 30f) + (mHandFloat * 0.5f)) * -1f;
        mHandMove = (mHand * 6f) * -1f;
        sHandMove = (sHand * 6) *-1;

        hourHandRect.Rotate(0, 0, hHandMove);
        minHandRect.Rotate(0, 0, mHandMove);
        secHandRect.Rotate(0, 0, sHandMove);
    }

    public void ShowSeconds(bool onOff)
    {
        if(onOff == true)
        {
            secHand.SetActive(true); 
            secText.SetActive(true); 
            secColon.SetActive(true);
            secToggle = true;
        }
        else if(onOff == false)
        {
            secHand.SetActive(false);
            secText.SetActive(false); 
            secColon.SetActive(false);
            secToggle = false;
        }
    }

    public void ToggleSeconds()
    {
        if(secToggle == true)
        {
            ShowSeconds(false);
        }
        else if(secToggle == false)
        {
            ShowSeconds(true);
        }
    }
}
