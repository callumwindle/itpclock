﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulkIncreaseScript : MonoBehaviour
{
    public GameObject clockScript, handsScript, buttonText;
    public int buttonPressed, currIncrament;
    private Text buttonDisplay;
    public string[] timeArray;
    
    // Start is called before the first frame update
    void Start()
    {
        timeArray = new string[6]{"1", "5", "10", "15", "30", "1"};
        currIncrament = 0;
        buttonDisplay = buttonText.GetComponent<Text>();
        buttonDisplay.text = (timeArray[currIncrament] + "Min");
    }

    public void updateIncrament()
    {
        currIncrament ++;
        if(currIncrament < 5)
        {
           buttonDisplay.text = (timeArray[currIncrament] + "Min"); 
        }
        else if(currIncrament == 5)
        {
            buttonDisplay.text = (timeArray[currIncrament] + "Hour"); 
        }
        else if(currIncrament > 5)
        {
            currIncrament = 0;
            buttonDisplay.text = (timeArray[currIncrament] + "Min"); 
        }
        
    }
    
}
