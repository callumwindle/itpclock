﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TimeScript : MonoBehaviour
{
    public GameObject hourDisplay, minDisplay, secDisplay, clockScript, multiButton, periodButton, resetButton, hsDigital;
    private HandsScript handsScript;
    private BulkIncreaseScript bulkScript;
    public string hourText, minText, secText;
    public int hourInt, minInt, secInt, buttonPressed;
    private int minTime = 0;
    private int indicator, incValue;
    private string timeNow;
    private string[] currTime;
    private int hHour, mMin, sSec;
    private bool flowTime, twelveHour, isHourIncrement, digitalDisplay;
    public float targetTime;
    public Image buttonDisplay;
    public Sprite playSprite, pauseSprite;
    
    // Start is called before the first frame update
    void Start()
    {
        flowTime = false;
        targetTime = 1f;
        twelveHour = false;
        digitalDisplay = true;
        SwitchTimePeriod();
        
        handsScript = clockScript.GetComponent<HandsScript>();
        bulkScript = multiButton.GetComponent<BulkIncreaseScript>();
        ResetTime();
        
    }

    void Update()
    {
        if(flowTime)
        {
            targetTime -= Time.deltaTime;
            if (targetTime <= 0.0f)
            {
                secInt ++;
                targetTime = 1f;
                EditTime(69);
            }
        }
    }

    public void ResetTime()
    {
        targetTime = 1f;
        if(twelveHour)
        {
            hourInt = 12;
            hourText = hourInt.ToString();
        }
        else
        {
            hourInt = 0;
            hourText = (hourInt.ToString() + "0");
        }
        minInt = 0;
        secInt = 0;

        
        minText = (minInt.ToString() + "0");
        secText = (secInt.ToString() + "0");

        hourDisplay.GetComponent<Text>().text = hourText;
        minDisplay.GetComponent<Text>().text = minText;
        secDisplay.GetComponent<Text>().text = secText;

        clockScript.GetComponent<HandsScript>().ResetHands();
    }

    void RefreshTime()
    {
        if (hourInt < 10)
        {
            hourText = ("0" + hourInt.ToString());
        }
        else if (hourInt >= 10 && hourInt < 24)
        {
            hourText = hourInt.ToString();
        }
       
        if (minInt < 10)
        {
            minText = ("0" + minInt.ToString());
        }
        else if (minInt >= 10 && minInt < 60)
        {
            minText = minInt.ToString();
        }

        if (secInt < 10)
        {
            secText = ("0" + secInt.ToString());
        }
        else if (secInt >= 10 && secInt < 60)
        {
            secText = secInt.ToString();
        }

        hourDisplay.GetComponent<Text>().text = hourText;
        minDisplay.GetComponent<Text>().text = minText;
        secDisplay.GetComponent<Text>().text = secText;
    }


    public void SetTimeNow(){
        timeNow = System.DateTime.Now.ToString("HH,mm,ss");
        currTime = timeNow.Split(',');
        hourInt = int.Parse(currTime[0]);
        if(twelveHour)
        {
            if(hourInt > 12)
            {
                hourInt -= 12;
            }
            else if(hourInt == 0)
            {
                hourInt = 12;
            }
        }
        minInt = int.Parse(currTime[1]);
        secInt = int.Parse(currTime[2]);

        RefreshTime();
        handsScript.MatchHands(hourInt, minInt, secInt);

    }

    public void EditTime(int buttonPressed)
    {
        indicator = buttonPressed;
        switch(indicator)
        {
            case 1:
                hourInt ++;
            break;

            case 2:
                hourInt --;
            break;

            case 3:
                minInt ++;
            break;

            case 4:
                minInt --;
            break;

            case 5:
                secInt ++;
            break;

            case 6:
                secInt --;
            break;

        }

        switch(secInt)
        {
            case (-1):
                secInt = 59;
                if(minInt == 0)
                {
                    minInt = -1;

                }
                else
                {
                    minInt --;
                }
                handsScript.MoveHands(4);
            break;
            case (60):
                secInt = minTime;
                minInt ++;
                handsScript.MoveHands(3);
            break;
        }

        switch(minInt)
        {
            case (-1):
                minInt = 59;
                if(hourInt == 0)
                {
                    if(!twelveHour)
                    {
                        hourInt = 23;
                    }
                    else
                    {
                        hourInt = 11;
                    }
                }
                else
                {
                    hourInt --;
                }
            break;
            case (60):
                minInt = minTime;
                hourInt ++;
            break;
        }
        
        switch(hourInt)
        {
            case (-1):
                if(!twelveHour)
                {
                    hourInt = 23;
                }
            break;
            case (0):
                if(twelveHour)
                {
                    hourInt = 12;
                }
            break;
            case (13):
                if(twelveHour)
                {
                    hourInt = 1;
                }
            break;
            case (24):
                if(!twelveHour)
                {
                    hourInt = minTime;
                }
            break;
        }

        RefreshTime();
        handsScript.MoveHands(buttonPressed);
    }

    public void BulkIncrease(int buttonPress)
    {
        incValue = int.Parse(bulkScript.timeArray[bulkScript.currIncrament]);
        isHourIncrement = false;
        
        if(incValue == 1 && bulkScript.currIncrament == 5)
        {
            incValue = 60;
            isHourIncrement = true;
        }
        
        switch(buttonPress)
        {
            case 1:
                if (minInt > (60 - incValue))
                {
                    minInt = (incValue - (60 - minInt));
                    hourInt ++;
                    if(twelveHour && hourInt > 12)
                    {
                        hourInt = 1;
                    }
                }
                else
                {
                    minInt += incValue;
                }
                
                if(!isHourIncrement)
                {
                    EditTime(7);
                }
                else
                {
                    EditTime(9);
                }
            break;

            case -1:
                if (minInt < (0 + incValue))
                {
                    minInt = (60 - (incValue - minInt));
                    hourInt --;
                    if(twelveHour && hourInt == 0)
                    {
                        hourInt = 12;
                    }
                }
                else
                {
                    minInt -= incValue;
                }
                
                
                if(!isHourIncrement)
                {
                    EditTime(8);
                }
                else
                {
                    EditTime(10);
                }
              break;
        }
        RefreshTime();
            
    }
    
    public void StartTime()
    {
        if(flowTime == true)
        {
            flowTime = false;
            buttonDisplay.sprite = playSprite;
            targetTime = 1f;
        }
        else
        {
            flowTime = true;
            buttonDisplay.sprite = pauseSprite;
            
        }
    }

    public void FlowTime()
    {
        DateTime now = System.DateTime.Now;

        while(flowTime == true)
        {
            
            while(System.DateTime.Now.Subtract(now).Seconds < 1)
            {
                //Just a while loop to time 1 second
                    
            }
            EditTime(5);
            break;
        }
    }

    public void SwitchTimePeriod()
    {
        twelveHour = !twelveHour;

        if(twelveHour)
        {
            if(hourInt > 12)
            {
                hourInt -= 12;
                RefreshTime();
            }
            else if(hourInt == 0)            
            {
                hourInt = 12;
                RefreshTime();
            }
            periodButton.GetComponent<Text>().text = "24Hr";
            resetButton.GetComponent<Text>().text = "12:00";
        }
        else
        {
            periodButton.GetComponent<Text>().text = "12Hr";
            resetButton.GetComponent<Text>().text = "00:00";
        }
    }

    public void HideDigital()
    {
        if(digitalDisplay)
        {
            hourDisplay.SetActive(false);
            minDisplay.SetActive(false);

            hsDigital.GetComponent<Text>().text = "Show Digital";

            if(handsScript.secToggle){
                secDisplay.SetActive(false);
            }

            digitalDisplay = false;
        }
        else
        {
            hourDisplay.SetActive(true);
            minDisplay.SetActive(true);

            hsDigital.GetComponent<Text>().text = "Hide Digital";

            if(handsScript.secToggle){
                secDisplay.SetActive(true);
            }

            digitalDisplay = true;
        }
    }
}
